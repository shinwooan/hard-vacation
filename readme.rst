OSAM : Vacation Manager
========================

국군 장병들은 늘 보이지 않는 곳에서 힘들게 일하고 있습니다. 비록 모든 것을 보상받을 수는 없겠지만, 그런 용사들을 위하여
여러가지 혜택들이 주어지고 있습니다. 영화관 입장료를 할인해주거나, 놀이공원을 무료로 입장할 수 있는 권한 등이 그것입니다.
그런 경우에, 본인의 군인 신분을 입증하기 위해서는 휴가증이 필요합니다. 본 프로젝트는 국군 장병들이 휴가증을 웹에 올리고 인증을
받음으로써, 휴가증 분실의 위협에서 자유로운 상태로 본인의 군인 신분을 인증받을 수 있도록 하는 것입니다.

# 프로젝트 구성

인증 방식 : passport-jwt 를 활용한 token형 인증.
표시 방식 : qrgenerator 를 이용한 qr코드 표시.


# 자주 사용되는 키워드
username : 신원 인증시에만 사용되는 키값입니다. 군번과 동일합니다.
password : 비밀번호입니다.
milnum : 군번입니다. 인증 외의 경우에는 milnum으로 호출됩니다.
id : DB의 인덱스 입니다.

# 서버
routes  : 
web : web asset을 포함합니다. web asset은 debuging을 위한 demo.js 파일과 QR코드 서비스를 위한 qr.js가 있습니다.

routes : 각종 API call에 대한 응답을 포함합니다.
 - login : 인증에 관련된 작업을 처리합니다. 
 - post : DB access에 관련된 작업을 처리합니다.
   /hero_check : 용사의 신원으로부터 해당 용사가 등록한 휴가증을 조회합니다.
   /hero_eelete : 특정 휴가증을 삭제합니다. 
   /manager_show : 간부의 신원으로부터 해당 간부에게 요청된 휴가증을 조회합니다.
   /manager_accept : 특정 휴가증을 승인합니다.
   /manager_deny : 특정 휴가증을 거부합니다.
   /register : 휴가증 승인을 요청합니다.

# 주의 사항
- 서버 이슈

  본 어플리케이션은 호스팅 서비스를 이용하지 않으면 dev-server를 활용합니다. 따라서 서버의 IP가 무작위적으로 변경될 수 있으며, 이 경우
  010-2936-1678 혹은 ungtong12@naver.com 으로 연락하여 변경된 서버 주소를 얻어야 합니다. 얻은 주소는 아래의 APK 파일의 값에 입력되어야 합니다.

  app/src/main/java/com/example/user/hradvacation/CommProtocol.java:35 >  String baseURL ="http://13.125.195.148:57728/";의 주소값.
  app/src/main/java/com/example/user/hradvacation/QRPopupActivity.java:46 > txttext.setText("http://13.125.195.148:57728/data?hash="+data); 의 주소값.

- DB 정보

  DB에는 아래와 같은 ID/PASSWORD 쌍이 저장되어 있습니다.

  병 : 17-76055082 / 960630 , 17-76074346 / 960327
  간부 : 11-00006 / 880808

  위 값을 활용하여 인증한 뒤 버튼을 눌러 각 기능을 활용해 보십시오.

- APK 위치
  
  app/release/app-release.apk

# 앱 화면
총 4개의 액티비티와 팝업들로 구성됩니다.
Activity1 : 메인 화면이자 로그인 화면입니다. 군번(username)과 생년월일(password)를 입력받습니다.
	버튼1(등록(병)) : Activity2로 이동합니다.
	버튼2(인증(간부)) : /manager_show 쿼리의 결과를 받아 Activity3으로 이동합니다.
	버튼3(확인) : /hero_check 쿼리의 결과를 받아Activity4로 이동합니다.
	
Activity2 : 휴가정보를 입력후 버튼을 누르면 /register 쿼리가 발생합니다.
	작업이 올바르게 진행됐는지 여부를 팝업으로 표시하고, Activity1로 다시 이동합니다.
	
Activity3 : 간부의 군번(manager_milnum)에 맞는 휴가증들을 /manager_show 의 결과로 출력합니다.
	각각의 요청을 누르면 버튼이 2개가 생성됩니다.
	버튼1(승인) : /manager_accept 쿼리와 id를 통해 휴가를 승인합니다.
	버튼2(거절) : /manager_deny 쿼리와 id를 통해 휴가를 거절합니다.
	버튼이 눌리면 해당 요청은 목록에서 사라지며, 이 작업의 성공여부를 팝업으로 표시합니다.
	
Activity4 : milnum에 맞는 휴가증들을 /hero_check 의 결과로써 출력합니다.
	각각의 요청을 누르면
	1. 휴가정보가 담긴 qr코드를 출력하는 팝업을 띄웁니다.
	2. 삭제 버튼이 생성됩니다.
	삭제버튼은 /hero_delete 쿼리와 id를 통해 db에서 휴가정보를 삭제하며, 그 여부를 팝업으로 표시합니다.
	

각각의 액티비티들은 .java 클래스들과 .xml 레이아웃, 그리고 필요에 따른 .java 어댑터 그리고 통신 프로토콜들로 구성됩니다.
파일들의 분류는 아래와 같습니다.

통신 프로토콜
	CommProtocol.java

Activity1
	MainActivity.java
	activity_main.xml

Activity2
	Register.java
	activity_register.xml

Activity3
	ManagerView.java
	activity_manager_view.xml
	manager_listview_item.xml

Activity4
	Userview.java
	activity_user_view.xml
	user_listview_item.xml

PopUp
	PopupActivity.java
	QRPopupActivity.java
	activity_popup.xml
	activity_qr_popup.xml
	NyQRcreater.java

Adapter(Activity 3, 4)
	UserListAdapter.java
	UserSHOWItemData.java
	ManagerListAdapter.java
	ManagerShowItemData.java
	ParsingJSON.java
	dialog_accept.xml