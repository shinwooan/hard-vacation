const express = require('express');
const router = express.Router();
var auth = require('../app/login/auth')();
const passport = require('passport');
var cfg = require('../jwt_config');
var mysql_db = require('../db/dbcon.js')();
var connection = mysql_db.init();
var jwt = require('jwt-simple');                                  
mysql_db.test_open(connection);

router.post('/login', function (req, res) {
	if (req.body.username && req.body.password) {
		var username = req.body.username;
		var password = req.body.password;
		var stmt = "SELECT password FROM login_table WHERE id = '"+username+"'";
		
		var setStatus = function(status){
			if (status) {
				var payload = {
					id: username
				};
				var token = jwt.encode(payload, cfg.jwtSecret);
				console.log(token);
				res.json({
					"lala" : "lalala",
					"state" : "true",
					"token": token
				});
			} else {
				res.json({"state" : "false", "message" : "Incorrect Password"});
			}
		};
		
		connection.query(stmt, function(err, result){
			if(err){
				console.log('id login error');
				setStatus(false);
			}
			if(result.length === 0){
				console.log("Non existing id");
				setStatus(false);
			}else{
				console.log("check", result[0].password, password);
				if(result[0].password == password){
					setStatus(true);
				}else{
					setStatus(false);
				}
			}
		});
	} else {
		res.json({"state" : "false", "message" : "Please send username & password"});
	}
});
// 클라이언트로부터 전송된 요청 헤더에 포함된 토큰으로부터 사용자 정보를 반환한다.

router.post('/logout', function(req, res){
	req.logout();
});

router.get('/info', auth.authenticateCatch(), function(req, res){
	res.send(req.user);
});

module.exports = router;