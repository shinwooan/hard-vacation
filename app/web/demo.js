const express = require('express');
const router = express.Router();
var mysql_db = require('../../db/dbcon.js')();
var connection = mysql_db.init();
var fs = require("fs");

mysql_db.test_open(connection);

router.get('/', function(req, res){
	fs.readFile(__dirname + '/template/index.html', function(err, data){
		if(err){
			console.log(err);
		}else{
			console.log("Access to web server");
			res.writeHead(200, {'Content-Type':'text/html', 'encoding' : 'utf-8'});
			res.write(data);
			res.end();		
		}
	});

});

module.exports = router;