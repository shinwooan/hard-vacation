const express = require('express');
const router = express.Router();
var mysql_db = require('../../db/dbcon.js')();
var connection = mysql_db.init();

mysql_db.test_open(connection);

router.get('/', function(req, res){	
	connection.query("SELECT *FROM vac_table WHERE hash = ?",[req.query.hash], function(err, result){
		if(err){
			console.log(err);
			res.send("NO data exist");
		}
		else{
			if(result.length){
				var datali = [["관등성명", result[0].username ],
							["군번", result[0].milnum],
							["휴가기간", result[0].start.substring(0,10) + " ~ " + result[0].end.substring(0,10)],
							["부대명", result[0].region ],
							["인증자", result[0].manager_milnum]];
				res.render("data.jade", {"datali" : datali});
			}else{
				res.send("NO data exist");			
			}
		}
	});
});
module.exports = router;