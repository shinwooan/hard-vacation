var passport = require('passport');
var passportJWT = require('passport-jwt');
var cfg = require('../../jwt_config');
var ExtractJwt = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;
var params = {
  // JWT 비밀키
  secretOrKey: cfg.jwtSecret,
  // 클라이언트에서 서버로 토큰을 전달하는 방식  (header, querystring, body 등이 있다.)
  // header 의 경우 다음과 같이 써야 한다 { key: 'Authorization', value: 'JWT' + 토큰
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};
console.log(cfg.jwtSecret);
module.exports = function () {
  var strategy = new Strategy(params, function (payload, done) {
    // TODO write authentications to find users from a database
      return done(null, {
        id: payload.id
      });
  });
  passport.use(strategy);
  return {
    initialize: function () {
      return passport.initialize();
    },
    authenticate: function () {
      return passport.authenticate('jwt', cfg.jwtSession);
    },
	authenticateCatch : function () {
      return (req, res) => {passport.authenticate('jwt', function(err, user, responce){
		  if(!responce.success){
			  res.status(401).json({ 
				message: 'Incorrect username and/or password'
			  });			  
		  }
		  return cfg.jwtSession;
	  })(req, res);
	};	
    },
	authenticateCheck : function () {
      return (req, res) => {
		  var retval = null;
		  var getter = function(param){
			  retval = param;
		  };
		  passport.authenticate('jwt', function(err, user){
		  if(err){
			  console.log(err);
		  }
		  if(!user){
			  getter(false);  
		  }else{
			  getter(user.id);
		  }
	  	})(req, res);
		  return retval;
	};
	}
  };
};