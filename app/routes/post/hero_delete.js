const express = require('express');

var mysql_db = require('../../../db/dbcon.js')();
var connection = mysql_db.init();
const router = express.Router();

var auth = require('../login/auth')();
mysql_db.test_open(connection);

router.post('/', function(req, res){
	var authInfo = auth.authenticateCheck()(req, res);	
	if(authInfo){
		connection.query("DELETE FROM vac_table WHERE id = ?", [req.body.id], function(err, result){
			if(err){
				console.log("Error at Query!");
				res.json({"success" : "false"});
			}
			else
				res.json({"state" : "true", "data" : result});
		});
	}else{
		res.json({"state" : "false"});
	}
});
module.exports = router;