var passport = require('passport');
var passportJWT = require('passport-jwt');
var cfg = require('../../jwt_config');
var ExtractJwt = passportJWT.ExtractJwt;
var Strategy = passportJWT.Strategy;


var params = {
	secretOrKey: cfg.jwtSecret,
	jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
};

var strategy = new Strategy(params, function (payload, done) {
	return done(null, {
		id: payload.id
	});
});
passport.use(strategy);

module.exports = function () {
	return {
		initialize: function () {
			return passport.initialize();
		},
		authenticate: function () {
			return passport.authenticate('jwt', cfg.jwtSession);
		},
		authenticateCatch : function () {
			return (req, res) => {passport.authenticate('jwt', function(err, user, responce){
				if(!responce.success){
					res.status(401).json({ 
						message: 'Incorrect username and/or password'
					});			  
				}
				return cfg.jwtSession;
			})(req, res);
			};	
		},
		authenticateCheck : function () {
			return (req, res) => {
				var retval = null;
				var getter = function(param){
					retval = param;
				};
				passport.authenticate('jwt', function(err, user){
					if(err || !user){
						getter(false);  
					}else{
						getter(user.id);
					}
				})(req, res);
			 return retval;
			};
		}
	};
};