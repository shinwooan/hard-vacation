var express = require("express");
var app = express();
var path = require('path');
var session =require('express-session');

var bodyParser = require('body-parser');
var passport = require('passport');

require('./routes/login/startLogin')(app);	//Initiate login module.

app.set('view-engine', 'jade');
app.set('views', './web/template');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app_post_urls = ['register', 'manager_show', 'manager_accept', 'hero_check', 'manager_deny', 'hero_delete'];

// POST URL connection
for(let urls of app_post_urls){
	app.use('/'+urls, require('./routes/post/' + urls));
}

//LOGIN connection
app.use('/auth', require('./auth_jwt'));

// Web service
app.use('/main', require('./web/demo'));
app.use('/data', require('./web/data'));
// Open port and start service
app.listen(3000, function(){
	console.log('connected');
});

